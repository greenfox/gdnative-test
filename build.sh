#!/bin/bash

main()
{
    setup
    build linux
}

setup()
{
    git submodule update --init --recursive
}


build()
{
plat=$1

pushd godot-cpp
scons platform=${plat} generate_bindings=yes use_custom_api_file=yes custom_api_file=../api.json bits=64 -j$(nproc)
popd

scons platform=${plat} -j$(nproc)
}


main